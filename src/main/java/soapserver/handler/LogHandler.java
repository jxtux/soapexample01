package soapserver.handler;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogHandler implements SOAPHandler<SOAPMessageContext> {

    static final Logger LOG = LogManager.getLogger(LogHandler.class);

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean procced = true;
        Boolean esSalida = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            SOAPMessage soapMessage = context.getMessage();

            //////////////HEADERRR///////////////
            SOAPEnvelope soapEnv = soapMessage.getSOAPPart().getEnvelope();
            SOAPHeader soapHeader = soapEnv.getHeader();
            Iterator itr = soapHeader.getAllAttributes();
            while (itr.hasNext()) {
                Object element = itr.next();
                LOG.info(element.toString());
            }
            
            ///// ***ESCRIBIR EN LOG******            
            soapMessage.writeTo(baos);
            LOG.info("{}=[{}]", esSalida ? "OUT" : "IN", baos.toString());
        } catch (Exception ex) {
            LOG.info("Error al capturar el mensaje", ex);
            procced = false;
        }
        return procced;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void close(MessageContext context) {
        // TODO Auto-generated method stub

    }

    @Override
    public Set<QName> getHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

    private void generateSOAPErrMessage(SOAPMessage msg, String reason) {
        try {
            SOAPBody soapBody = msg.getSOAPPart().getEnvelope().getBody();
            SOAPFault soapFault = soapBody.addFault();
            soapFault.setFaultString(reason);
            throw new SOAPFaultException(soapFault);
        } catch (SOAPException e) {
        }
    }
}

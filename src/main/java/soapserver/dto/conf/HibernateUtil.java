package soapserver.dto.conf;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import soapserver.dto.model.Countries;
import soapserver.dto.model.Departments;
import soapserver.dto.model.Employees;
import soapserver.dto.model.JobHistory;
import soapserver.dto.model.JobHistoryPK;
import soapserver.dto.model.Jobs;
import soapserver.dto.model.Locations;
import soapserver.dto.model.Regions;
import soapserver.dto.model.UserRoles;
import soapserver.dto.model.Users;

public class HibernateUtil {

   private static StandardServiceRegistry registry;
   private static SessionFactory sessionFactory;

   public static SessionFactory getSessionFactory() {
      if (sessionFactory == null) {
         try {
            StandardServiceRegistryBuilder registryBuilder =   new StandardServiceRegistryBuilder();

            Map<String, Object> settings = new HashMap<>();
            settings.put(Environment.DRIVER, "oracle.jdbc.OracleDriver");
            settings.put(Environment.URL, "jdbc:oracle:thin:@localhost:1521:XE");
            settings.put(Environment.USER, "hr");
            settings.put(Environment.PASS, "oracle");
            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, true);

            // HikariCP settings
            
            // Maximum waiting time for a connection from the pool
            settings.put("hibernate.hikari.connectionTimeout", "20000");
            // Minimum number of ideal connections in the pool
            settings.put("hibernate.hikari.minimumIdle", "10");
            // Maximum number of actual connection in the pool
            settings.put("hibernate.hikari.maximumPoolSize", "20");
            // Maximum time that a connection is allowed to sit ideal in the pool
            settings.put("hibernate.hikari.idleTimeout", "300000");

            registryBuilder.applySettings(settings);

            registry = registryBuilder.build();
            MetadataSources sources = new MetadataSources(registry)
                    .addAnnotatedClass(Employees.class)
                    .addAnnotatedClass(Countries.class)
                    .addAnnotatedClass(JobHistory.class)
                    .addAnnotatedClass(JobHistoryPK.class)
                    .addAnnotatedClass(Jobs.class)
                    .addAnnotatedClass(Locations.class)
                    .addAnnotatedClass(Regions.class)                    
                    .addAnnotatedClass(Departments.class)
                    .addAnnotatedClass(Users.class)
                    .addAnnotatedClass(UserRoles.class);
            
            Metadata metadata = sources.getMetadataBuilder().build();
            
            
            sessionFactory = metadata.getSessionFactoryBuilder().build();
         } catch (Exception e) {
            if (registry != null) {
               StandardServiceRegistryBuilder.destroy(registry);
            }
            e.printStackTrace();
         }
      }
      return sessionFactory;
   }

   public static void shutdown() {
      if (registry != null) {
         StandardServiceRegistryBuilder.destroy(registry);
      }
   }
}


package soapserver.dto;

public class SimpleExceptionBean {

    private String message;

    public SimpleExceptionBean() {
    }

    public SimpleExceptionBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

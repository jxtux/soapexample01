package soapserver.dto;

import java.util.Date;

public class User {

    private Integer id;
    private String Name;
    private Date edad;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Date getEdad() {
        return edad;
    }

    public void setEdad(Date edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", Name=" + Name + ", edad=" + edad + '}';
    }
    
}

package soapserver.dto.exception;

import javax.xml.ws.WebFault;
import soapserver.dto.SimpleExceptionBean;

@WebFault(name = "SimpleException")
public class SimpleException extends Exception {

    private SimpleExceptionBean faultBean;

    public SimpleException(String message, SimpleExceptionBean faultInfo) {
        super(message);
        faultBean = faultInfo;
    }

    public SimpleException(String message, SimpleExceptionBean faultInfo, Throwable cause) {
        super(message, cause);
        faultBean = faultInfo;
    }

    public SimpleExceptionBean getFaultInfo() {
        return faultBean;
    }
}

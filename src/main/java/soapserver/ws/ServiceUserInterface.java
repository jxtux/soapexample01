package soapserver.ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import soapserver.dto.User;
import soapserver.dto.exception.SimpleException;

@WebService(name = "ServiceUser")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public interface ServiceUserInterface {

    @WebMethod(operationName = "obtenerMessageName", action = "obtenerMessageNameAction")
    public String getMessage(@WebParam(name = "name") String txt) throws SimpleException;

    @WebMethod(operationName = "getUserParametroObject")
    public User getUserParametroObject(@WebParam(name = "user") User user);

    @WebMethod(operationName = "getListUser")
    public List<User> getListUser(@WebParam(name = "cant") Integer cantidad);

}

package soapserver.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "ServiceNota")
public interface ServiceNotaInterface {

    @WebMethod(operationName = "subirNota")
    public Double getSubirNota(@WebParam(name = "nota") Double nota);


}

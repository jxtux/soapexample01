package soapserver.ws.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import soapserver.dto.SimpleExceptionBean;
import soapserver.dto.User;
import soapserver.dto.conf.HibernateUtil;
import soapserver.dto.exception.SimpleException;
import soapserver.dto.model.Employees;
import soapserver.ws.ServiceUserInterface;

@WebService(endpointInterface = "soapserver.ws.ServiceUserInterface")
@HandlerChain(file = "/handler-chain.xml")
public class ServiceUserImpl implements ServiceUserInterface {

    static final Logger LOG = LogManager.getLogger(ServiceUserImpl.class);

    @Override
    public String getMessage(String name) throws SimpleException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.getTransaction();
            transaction.begin();

            List<Object[]> listE = session.createQuery(
                    "select e.employeeId,e.firstName from Employees e where e.phoneNumber = :phoneNumber and e.jobId.jobId = :jobId")
                    .setParameter("phoneNumber", "99999999")
                    .setParameter("jobId", "AD_PRES")
                    .list();

            for (Object[] object : listE) {
                LOG.info("id:"+object[0].toString()+",name:"+object[1].toString());
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }

        HibernateUtil.shutdown();

        if (name.equalsIgnoreCase("xxx")) {
            LOG.error("Valor no valido");
            throw new SimpleException("Fault Detected", new SimpleExceptionBean());
        }

        return "Hello " + name + " !";
    }

    @Override
    public User getUserParametroObject(User user) {
        User u = new User();
        u.setId(user.getId());
        u.setName(user.getName());
        u.setEdad(user.getEdad());

        return u;
    }

    @Override
    public List<User> getListUser(Integer cantidad) {

        List<User> list = new ArrayList<>();

        for (int i = 0; i < cantidad; i++) {
            User u = new User();
            u.setId(i);
            u.setName("name" + i);
            u.setEdad(new Date());

            list.add(u);
        }

        return list;
    }

}

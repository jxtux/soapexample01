package soapserver.ws.impl;

import javax.jws.WebService;
import soapserver.ws.ServiceNotaInterface;

@WebService(endpointInterface = "soapserver.ws.ServiceNotaInterface")
public class NotaUserImpl implements ServiceNotaInterface {

    @Override
    public Double getSubirNota(Double nota) {
        nota = (nota < 20) ? 20.00 : nota + 1.0;

        return nota;
    }

}
